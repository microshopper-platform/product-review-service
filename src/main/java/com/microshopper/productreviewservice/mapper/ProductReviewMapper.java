package com.microshopper.productreviewservice.mapper;

import com.microshopper.productreviewservice.model.ProductReview;
import com.microshopper.productreviewservice.model.Rating;
import com.microshopper.productreviewservice.web.dto.ProductReviewDto;
import com.microshopper.productreviewservice.web.dto.RatingDto;
import org.springframework.stereotype.Component;

@Component
public class ProductReviewMapper {

  public ProductReviewDto mapTo(ProductReview productReview) {

    ProductReviewDto productReviewDto = new ProductReviewDto();
    productReviewDto.setId(productReview.getId());
    productReviewDto.setProductId(productReview.getProductId());
    productReviewDto.setUsername(productReview.getUsername());
    productReviewDto.setTitle(productReview.getTitle());
    productReviewDto.setRating(RatingDto.valueOf(productReview.getRating().toString()));
    productReviewDto.setDescription(productReview.getDescription());

    return productReviewDto;
  }

  public ProductReview mapFrom(ProductReviewDto productReviewDto) {

    ProductReview productReview = new ProductReview();
    productReview.setProductId(productReviewDto.getProductId());
    productReview.setUsername(productReviewDto.getUsername());
    productReview.setTitle(productReviewDto.getTitle());
    productReview.setRating(Rating.valueOf(productReviewDto.getRating().toString()));
    productReview.setDescription(productReviewDto.getDescription());

    return productReview;
  }
}
