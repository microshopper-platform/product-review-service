package com.microshopper.productreviewservice.web.controller;

import com.microshopper.productreviewservice.service.ProductReviewService;
import com.microshopper.productreviewservice.web.dto.ProductReviewDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "productReview", description = "The Product Review API")
public class ProductReviewController {

  private static final String BASE_URL = "/api/product-reviews";

  private final ProductReviewService productReviewService;

  @Autowired
  public ProductReviewController(ProductReviewService productReviewService) {
    this.productReviewService = productReviewService;
  }

  @GetMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.OK)
  public List<ProductReviewDto> getAllReviews() {

    return productReviewService.getAllReviews();
  }

  @GetMapping(value = BASE_URL + "/product/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public List<ProductReviewDto> getAllProductReviews(@PathVariable(name = "id") Long productId) {

    return productReviewService.getAllProductReviews(productId);
  }

  @GetMapping(value = BASE_URL + "/account/{username}")
  @ResponseStatus(value = HttpStatus.OK)
  public List<ProductReviewDto> getAllAccountReviews(@PathVariable(name = "username") String username) {

    return productReviewService.getAllAccountReviews(username);
  }

  @GetMapping(value = BASE_URL + "/account/{username}/product/{productId}")
  @ResponseStatus(value = HttpStatus.OK)
  public ProductReviewDto getAccountProductReview(
    @PathVariable(name = "username") String username,
    @PathVariable(name = "productId") Long productId) {

    return productReviewService.getAccountProductReview(username, productId);
  }

  @PostMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.CREATED)
  public ProductReviewDto addProductReview(@RequestBody ProductReviewDto productReviewDto) {

    return productReviewService.addProductReview(productReviewDto);
  }

  @PutMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateProductReview(@PathVariable Long id, @RequestBody ProductReviewDto productReviewDto) {

    productReviewService.updateProductReview(id, productReviewDto);
  }

  @DeleteMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public void deleteProductReview(@PathVariable Long id) {

    productReviewService.deleteProductReview(id);
  }
}
