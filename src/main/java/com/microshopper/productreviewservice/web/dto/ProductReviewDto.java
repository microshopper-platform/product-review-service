package com.microshopper.productreviewservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "ProductReview")
public class ProductReviewDto {

  private Long id;

  private Long productId;

  private String username;

  private String title;

  private RatingDto rating;

  private String description;
}
