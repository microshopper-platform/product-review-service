package com.microshopper.productreviewservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "Rating")
public enum RatingDto {

  EXCELLENT,

  VERY_GOOD,

  GOOD,

  BAD,

  VERY_BAD,
}
