package com.microshopper.productreviewservice.exception;

public class ProductReviewNotFoundException extends RuntimeException {

  public ProductReviewNotFoundException(String message) {
    super(message);
  }
}
