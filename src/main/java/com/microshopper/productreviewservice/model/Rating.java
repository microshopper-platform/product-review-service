package com.microshopper.productreviewservice.model;

public enum Rating {

  EXCELLENT,

  VERY_GOOD,

  GOOD,

  BAD,

  VERY_BAD,
}
