package com.microshopper.productreviewservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name = "product_review")
public class ProductReview {

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "product_id")
  private Long productId;

  @Column(name = "username")
  private String username;

  @Column(name = "title")
  private String title;

  @Column(name = "rating")
  @Enumerated(EnumType.STRING)
  private Rating rating;

  @Column(name = "description")
  private String description;
}
