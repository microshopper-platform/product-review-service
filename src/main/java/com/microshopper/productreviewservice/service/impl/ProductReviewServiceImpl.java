package com.microshopper.productreviewservice.service.impl;

import com.microshopper.productreviewservice.exception.ProductReviewNotFoundException;
import com.microshopper.productreviewservice.mapper.ProductReviewMapper;
import com.microshopper.productreviewservice.model.ProductReview;
import com.microshopper.productreviewservice.model.Rating;
import com.microshopper.productreviewservice.repository.ProductReviewRepository;
import com.microshopper.productreviewservice.service.ProductReviewService;
import com.microshopper.productreviewservice.web.dto.ProductReviewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductReviewServiceImpl implements ProductReviewService {

  private final ProductReviewRepository productReviewRepository;
  private final ProductReviewMapper productReviewMapper;

  @Autowired
  public ProductReviewServiceImpl(
    ProductReviewRepository productReviewRepository,
    ProductReviewMapper productReviewMapper) {

    this.productReviewRepository = productReviewRepository;
    this.productReviewMapper = productReviewMapper;
  }

  @Override
  public List<ProductReviewDto> getAllReviews() {

    List<ProductReview> productReviews = productReviewRepository.findAll();

    return productReviews.stream().map(productReviewMapper::mapTo).collect(Collectors.toList());
  }

  @Override
  public List<ProductReviewDto> getAllProductReviews(Long productId) {

    List<ProductReview> productReviews = productReviewRepository.getAllByProductId(productId);

    return productReviews.stream().map(productReviewMapper::mapTo).collect(Collectors.toList());
  }

  @Override
  public List<ProductReviewDto> getAllAccountReviews(String username) {

    List<ProductReview> productReviews = productReviewRepository.getAllByUsername(username);

    return productReviews.stream().map(productReviewMapper::mapTo).collect(Collectors.toList());
  }

  @Override
  public ProductReviewDto getAccountProductReview(String username, Long productId) {

    Optional<ProductReview> productReview =
      productReviewRepository.getProductReviewByUsernameAndProductId(username, productId);

    if (productReview.isPresent()) {
      return productReviewMapper.mapTo(productReview.get());
    } else {
      throw new ProductReviewNotFoundException("Product review for productId: " + productId + " from username: " + username + " not found.");
    }
  }

  @Override
  public ProductReviewDto addProductReview(ProductReviewDto productReviewDto) {

    ProductReview productReview = productReviewMapper.mapFrom(productReviewDto);
    ProductReview newProductReview = productReviewRepository.save(productReview);
    return productReviewMapper.mapTo(newProductReview);
  }

  @Override
  public void updateProductReview(Long id, ProductReviewDto productReviewDto) {

    Optional<ProductReview> productReviewToBeUpdated = productReviewRepository.findById(id);
    productReviewToBeUpdated.ifPresentOrElse(productReview -> {
      productReview.setRating(Rating.valueOf(productReviewDto.getRating().toString()));
      productReview.setDescription(productReviewDto.getDescription());
      productReviewRepository.save(productReview);
    }, () -> { throw new ProductReviewNotFoundException("Product review update failed. Product review with id: " + id + " not found");
    });
  }

  @Override
  public void deleteProductReview(Long id) {

    productReviewRepository.deleteById(id);
  }
}
