package com.microshopper.productreviewservice.service;

import com.microshopper.productreviewservice.web.dto.ProductReviewDto;

import java.util.List;

public interface ProductReviewService {

  List<ProductReviewDto> getAllReviews();

  List<ProductReviewDto> getAllProductReviews(Long productId);

  List<ProductReviewDto> getAllAccountReviews(String username);

  ProductReviewDto getAccountProductReview(String username, Long productId);

  ProductReviewDto addProductReview(ProductReviewDto productReviewDto);

  void updateProductReview(Long id, ProductReviewDto productReviewDto);

  void deleteProductReview(Long id);
}
