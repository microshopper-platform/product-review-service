CREATE TABLE product_review (
    id serial NOT NULL,
    product_id integer NOT NULL,
    user_id integer NOT NULL,
    rating character varying(32) NOT NULL,
    description character varying(2048),
    PRIMARY KEY (id)
);
